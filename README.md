# Tableau - Managerial Dashboard

## Table of Contents

- 1.How to open the project?
- [2.Link to the project ](https://public.tableau.com/app/profile/michal.w7301/viz/KokpitMenederkistudiumprzypadkuMichaKondrat-Wrbel95664/FINAL)
- 3.Project description
- 4.Data sources 
- 5.Data model 

## 1. How to open the project?

To run the project on your platform, please follow these steps.
#### Step 1
![Alt text](image-10.png)

#### Step 2
![](image-2.png)

#### Step 3

![Step 3](image-11.png)


#### Step 4

![](image-7.png)


#### Step 5
![](image-4v2.png)


#### Step 6
![](image-8.png)

#### Step 7

![](image-9.png)


#### Step 8
![step 8](image-1.png)


## 2. Link to project 

[Tableau Project](https://public.tableau.com/app/profile/michal.w7301/viz/KokpitMenederkistudiumprzypadkuMichaKondrat-Wrbel95664/FINAL)

## 3. Project Description

The project was created for the purpose of completing a master's thesis in the field of Data Analysis - Big Data: <i> "Applying Business Intelligence Technology as a Support System for Business Decision-Making in an Organization"</i>. The objective of the project is to present a managerial dashboard as a tool to support decision-making processes within an organization. The project was developed using Tableau software.


## 4. Data Sources

The company collects all financial data and information related to order fulfillment status within the Comarch Optima system (Commercial Book Module) as well as in Excel spreadsheets.The analyzed data covers the period from 2017 to 2020 and comprises a total of 11,576 entries, representing the number of completed sales transactions for the product.

## 5. Data model

Due to the structured nature of the problems and the analyzed data, a relational database has been designed. This database encompasses both the order fulfillment process by customers and stores information about the organization's employees.

![Data model](image.png)


